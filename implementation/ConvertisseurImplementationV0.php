<?php

namespace App\implementation;

use App\Devise;


class ConvertisseurImplementationV0 extends ConvertisseurComposant implements InterfaceOfferte{


    public function __construct()
    {

        $this->EUR = new Devise('EURO', 1, 'EUR');
        $this->USD = new Devise('USD', 1, 'USD');
        $this->JPY = new Devise('JPY', 1, 'JPY');
        $this->GBP = new Devise('GBP', 1, 'GBP');
        $this->CNY = new Devise('CNY', 1, 'CNY');

    }

    public function getListDevise(){


       return $monnaies = array($this->EUR, $this->USD, $this->JPY, $this->GBP, $this->CNY);

    }

    public function convert($DeviseFrom, $DeviseTo, $Montant){



    }

    public function convertWithCode($codeDeviseFrom, $codeDeviseTo, $montant){



    }



}