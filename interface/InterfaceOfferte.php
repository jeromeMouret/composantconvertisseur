<?php

namespace App;


interface InterfaceOfferte{


    public function getListDevise();

    public function convert($DeviseFrom, $DeviseTo, $Montant);

    public function convertWithCode($codeDeviseFrom, $codeDeviseTo, $montant);


}

