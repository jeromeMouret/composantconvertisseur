<?php

 namespace App;


abstract class ConvertisseurComposant{

    public  $unPortOffert;

    public function setPortOffert (InterfaceOfferte $portOffert){

        $this->unPortOffert = $portOffert;

    }

    public function getPortOffert (){

       return $this->unPortOffert;

    }

    
}
